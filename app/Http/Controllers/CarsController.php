<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCarRequest;
use App\Http\Requests\UpdateCarRequest;
use App\Models\Car;
use App\Models\Notification;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Car::latest()->paginate(15);
        return view('cars.index', compact('cars'));
    }

    /**
     * Display a listing of the bookmarked cars.
     *
     * @return \Illuminate\Http\Response
     */
    public function bookmarks()
    {
        $cars = Car::where('is_bookmarked', true)->paginate(24);
        return view('cars.index', compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cars = Car::search();

        if ($cars) {
            Notification::new_cars($cars);
        }

        return view('cars.index', compact('cars'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCarRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCarRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        return view('cars.show', compact('car'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCarRequest  $request
     * @param  \App\Models\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCarRequest $request, Car $car)
    {
        //
    }

    /**
     * Bookmark a car.
     *
     * @param  \App\Models\Car  $car
     * @return array
     */
    public function bookmark(int $id): array
    {
        $car = Car::find($id);
        $car->is_bookmarked = !$car->is_bookmarked;
        $car->save();
        return [
            'car' => $car->title,
            'ad_id' => $car->ad_id,
            'is_bookmarked' => $car->is_bookmarked
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        //
    }

    public function fetch_details($url = null)
    {
        $car = Car::where('is_details_fetched', 0)->orderBy('created_at', 'desc')->first();
        if ($car)
            $detailed = Car::fetch_details($car->ad_id);
        else
            return 'All cars up to date!';

        if ($detailed) {
            $car->is_details_fetched = true;
            $car->save();
            return $car;
        } else {
            return $detailed;
        }
    }
}
