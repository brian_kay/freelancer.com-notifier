<?php

namespace App\Console\Commands;

use App\Models\Farm;
use App\Models\Notification;
use Illuminate\Console\Command;

class FetchFarms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'farms:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch new farms listed on AdsAfrica';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $farms = Farm::fetch();

        if ($farms)
            Notification::new_farms($farms);

        return Command::SUCCESS;
    }
}
