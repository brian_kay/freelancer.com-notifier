<?php

namespace App\Console\Commands;

use App\Models\Car;
use App\Models\Notification;
use Illuminate\Console\Command;

class FetchCars extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cars:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch new cars listed on AdsAfrica';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cars = Car::search();

        if ($cars)
            Notification::new_cars($cars);

        return Command::SUCCESS;
    }
}
