<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Notification;
use App\Models\Project;
use App\Models\Client;

class CheckProjects extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'projects:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for new projects and email if any new are found';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $result = Project::fetch()->result;

        $clients = Client::process($result->users);
        $projects = Project::process($result->projects);

        if ($projects) {
            Notification::new_projects($projects);
        }

        return Command::SUCCESS;
    }
}
