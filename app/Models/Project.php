<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $guarded = []; // TODO Use $fillable

    public static function fetch()
    {
        $url = "https://www.freelancer.com/api/projects/0.1/projects/active?limit=20&full_description=true&job_details=true&local_details=true&location_details=true&upgrade_details=true&user_country_details=true&user_details=true&user_employer_reputation=true&user_status=true&countries[]=za&jobs[]=3&jobs[]=305&jobs[]=669&jobs[]=1092&languages[]=en&sort_field=submitdate&webapp=1&compact=true&new_errors=true&new_pools=true";
        $client = new \GuzzleHttp\Client();
        $res = $client->get($url, ['timeout' => 5])->getBody();
        $json = $res->getContents();

        return  json_decode($json);
    }

    public static function process($data): array
    {
        $new_projects = [];
        foreach ($data as $p) {
            $project = self::firstOrCreate(
                [
                    'fl_id' => $p->id
                ],
                [
                    'title' => $p->title,
                    'preview_description' => $p->preview_description,
                    'description' => $p->description,
                    'currency' => $p->currency->code,
                    'budget_min' => $p->budget->minimum,
                    'budget_max' => $p->budget->maximum ?? 0,
                    'deadline' => $p->submitdate,
                    'client_id' => $p->owner_id,
                    'time_submitted' => $p->time_submitted,
                    'time_updated' => $p->time_updated,
                ]
            );
            if ($project->wasRecentlyCreated) {
                $new_projects[$project->fl_id] = $project;
            }
        }
        return $new_projects;
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'fl_id');
    }
}
