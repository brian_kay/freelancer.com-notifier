<?php

namespace App\Models;

use App\Mail\NewCars;
use App\Mail\NewFarms;
use App\Mail\NewProjects;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Notification extends Model
{

    public static function new_projects(array $projects)
    {
        Mail::to(env('MAIL_ADMIN_ADDRESS'), env('MAIL_ADMIN_NAME'))->send(new NewProjects($projects));
    }

    public static function new_cars(array $cars)
    {
        Mail::to(env('MAIL_ADMIN_ADDRESS'), env('MAIL_ADMIN_NAME'))->send(new NewCars($cars));
    }

    public static function new_farms(array $farms)
    {
        Mail::to(env('MAIL_ADMIN_ADDRESS'), env('MAIL_ADMIN_NAME'))->send(new NewFarms($farms));
    }
}
