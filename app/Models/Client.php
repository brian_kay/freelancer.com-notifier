<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $guarded = []; // TODO Use fillable

    public static function process($data): array
    {
        $new_clients = [];
        foreach ($data as $c) {
            $client = self::firstOrCreate(
                [
                    'fl_id' => $c->id
                ],
                [
                    'registration_date' => $c->registration_date,
                    'username' => $c->username,
                    'public_name' => $c->public_name,
                    'display_name' => $c->display_name,
                    'country' => $c->location->country->code,
                    'city' => $c->location->city ?? '',
                    'reputation' => $c->employer_reputation->entire_history->overall,
                    'status' => json_encode($c->status),
                ]
            );
            if ($client->wasRecentlyCreated) {
                $new_projects[$client->fl_id] = $client;
            }
        }
        return $new_clients;
    }

    public function projects()
    {
        return $this->hasMany(Project::class, 'fl_id', 'client_id');
    }
}
