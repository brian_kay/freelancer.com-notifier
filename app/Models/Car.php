<?php

namespace App\Models;

use App\Casts\Json;
use DiDom\Document;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class Car extends Model
{
    use HasFactory, SoftDeletes;
    protected $guarded = [];

    protected $casts = [
        'images' => Json::class,
    ];

    protected $primaryKey = 'ad_id';

    /**
     * Fetch latest listings that meet a given set of rules
     * @return array
     * @throws \DiDom\Exceptions\InvalidSelectorException
     */
    public static function search()
    {

        $urls = [
            'https://www.gumtree.co.za/s-cars-bakkies/v1c9077p1?cy=2014,&pr=2000,39000&st=ownr&priceType=FIXED',
            'https://www.gumtree.co.za/s-cars-bakkies/eastern-cape/v1c9077l3100197p1?pr=2000,39000&st=ownr&priceType=FIXED'
        ];

        $cars = self::search_gumtree($urls);

        return $cars;
    }

    /**
     * Fetch latest listings that meet a given set of rules
     * 
     * @param array $urls
     * @return array
     * @throws \DiDom\Exceptions\InvalidSelectorException
     */
    public static function search_gumtree(array $urls)
    {
        $cars = [];
        foreach ($urls as $url) {
            $html = http_get($url)->body();
            $document = new Document();
            $document->loadHtml($html);
            $date = null;
            $items = $document->find('.related-item');

            foreach ($items as $index => $item) {
                $thumbnail = @$item->find('.bolt-img>img');
                $thumbnail = $thumbnail ? $thumbnail[0]->attr('data-src') : null;
                $link = @$item->find('.related-ad-title')[0];
                $link = 'https://www.gumtree.co.za' . $link->attr('href');
                $year = @$item->find('.chiplets-container>span')[0]->text();
                $price = str_to_int($item->find('.ad-price')[0]->text());
                $ad_id = @$item->attr('data-adid');
                $date = @$item->find('.creation-date')[0]->text();
                $date = carbon()->sub(strtr($date, ['min' => 'minutes', 'mo' => 'months']));

                $car = self::firstOrCreate(
                    ['ad_id' => $ad_id],
                    [
                        'price' => $price,
                        'title' => remove_whitespace(@$item->find('.related-ad-title')[0]->text()),
                        'location' => remove_whitespace(@$item->find('.location')[0]->text()),
                        'url' => $link,
                        'year' => $year,
                        'thumbnail' => $thumbnail,
                        'posted_at' => $date,
                        'mileage' => str_to_int(@$item->find('.chiplets-container>span')[1]?->text()),
                        'seller_name' => @$item->attr('data-seller-name'),
                    ]
                );
                if ($car->wasRecentlyCreated)
                    $cars[] = $car;
            }
            sleep(rand(2, 9));
        }
        return $cars;
    }

    /**
     * Fetch latest listings that meet a given set of rules
     * 
     * @param array $urls
     * @return array
     * @throws \DiDom\Exceptions\InvalidSelectorException
     */
    public static function search_adsafrica(array $urls)
    {
        $cars = [];
        foreach ($urls as $url) {
            $html = http_get($url)->body();
            $document = new Document();
            $document->loadHtml($html);
            $date = null;
            $items = $document->find('tr');

            foreach ($items as $index => $item) {
                if ($item->classes()->getAll()[0] === 'ILDate') {
                    $date = date('Y-m-d H:i:s', strtotime((remove_whitespace($item->text()))));
                    continue;
                }
                $thumbnail = @$item->find('noscript>img');
                $thumbnail = $thumbnail ? str_replace('t.jpg', '.jpg', $thumbnail[0]->attr('src')) : null;
                $link = @$item->find('.list_item_title')[0];
                $year = null;
                preg_match('/(20[0-9]{2})/', $link->text(), $year);
                if (!$year || !$item->find('.card-price'))
                    continue;
                $year = $year[1];
                $price = str_to_int($item->find('.card-price')[0]->text());
                $ad_id = trim(explode('/item', $link->attr('href'))[1], '/');

                if (($price > 30000 && $year < 2017) || $price > 75000)
                    continue;

                $car = self::firstOrCreate(
                    ['ad_id' => $ad_id],
                    [
                        'price' => $price,
                        'title' => remove_whitespace($link->text()),
                        'description' => remove_whitespace(nl2br($item->find('.list_item_description')[0]->innerHtml())),
                        'location' => remove_whitespace($item->find('.list_item_path')[0]->text()),
                        'url' => $link->attr('href'),
                        'year' => $year,
                        'thumbnail' => $thumbnail,
                        'posted_at' => $date
                    ]
                );
                if ($car->wasRecentlyCreated)
                    $cars[] = $car;
            }
            sleep(rand(2, 9));
        }
        return $cars;
    }

    public static function fetch_gumtree(string|int $string)
    {
        $images = [];

        if (str_contains($string, 'https')) {
            $ad_id = trim(explode('/item', $string)[1], '/');
            $url = $string;
        } else {
            $ad_id = $string;
            $url = 'https://gumtree.co.za/g/' . $string;
        }

        $car = Car::where('ad_id', $ad_id)->first();

        if(!$car)
            return false;

        return $car; // TODO: Fetch details

        try {
            $html = http_get($car->url)->body();
            $document = new Document();
            $document->loadHtml($html);
        } catch (\RuntimeException $e) {
            if ($car)
                $car->delete();
            return $e->getMessage();
        }

        if ($document->find('.validationError')) {
            if ($car)
                $car->delete();
            return 'Car deleted';
        }

        $title = explode(' - ', $document->find('title')[0]->text())[0];
        $price = str_to_int($document->find('#price>.params_field_value')[0]->text());
        $description = remove_whitespace($document->find('#item_text_value')[0]->innerHtml());
        $location = self::field_value($document, '#city_name>.params_field_value');
        $year = self::field_value($document, '#field_custom_year>.params_field_value');
        $mileage = str_to_int(self::field_value($document, '#field_custom_milage>.params_field_value'));
        $body_type = self::field_value($document, '#field_custom_bodyType>.params_field_value');
        $transmission = self::field_value($document, '#field_custom_transmission>.params_field_value');
        $fuel_type = self::field_value($document, '#field_custom_engineType>.params_field_value');
        $drive_type = self::field_value($document, '#field_custom_driveType>.params_field_value');
        $seller_name = self::field_value($document, '#contact_name>#contact_field_value');
        $seller_phone = self::field_value($document, '#contact_phone>#contact_field_value');
        $link = $document->find('link')[0]->attr('href');
        $thumbnail = @$document->find('#mainPic');
        $thumbnail = $thumbnail ? $thumbnail[0]->attr('src') : null;
        $thumbs = $document->find('img.thumb');
        foreach ($thumbs as $thumb) {
            $remote = str_replace('t.jpg', '.jpg', $thumb->attr('src'));
            $path = 'cars/' . $ad_id . '_' . substr($remote, -5);
            if (Storage::disk()->put($path, Http::get($remote)->body())) {
                $images[] = "storage/$path";
            }
        }
        $posted_at = carbon(self::field_value($document, '#item_date'));

        $car_details =  [
            'title' => $title,
            'price' => $price,
            'description' => $description,
            'location' => $location,
            'year' => $year,
            'mileage' => $mileage,
            'body_type' => $body_type,
            'transmission' => $transmission,
            'fuel_type' => $fuel_type,
            'drive_type' => $drive_type,
            'seller_name' => substr($seller_name, 0, 31),
            'seller_phone' => $seller_phone,
            'url' => $link,
            'thumbnail' => $thumbnail,
            'images' => $images,
            'is_details_fetched' => true,
            'posted_at' => $posted_at,
        ];

        $car = self::firstOrCreate(['ad_id' => $ad_id], $car_details);
        $car->update($car_details);
        return $car;
    }

    public static function fetch_adsafrica(string|int $string)
    {
        $images = [];

        if (str_contains($string, 'https')) {
            $ad_id = trim(explode('/item', $string)[1], '/');
            $url = $string;
        } else {
            $ad_id = $string;
            $url = 'https://adsafrica.co.za/item/' . $string;
        }

        $car = Car::where('ad_id', $ad_id)->first();

        try {
            $html = http_get($url)->body();
            $document = new Document();
            $document->loadHtml($html);
        } catch (\RuntimeException $e) {
            if ($car)
                $car->delete();
            return $e->getMessage();
        }

        if ($document->find('.validationError')) {
            if ($car)
                $car->delete();
            return 'Car deleted';
        }

        $title = explode(' - ', $document->find('title')[0]->text())[0];
        $price = str_to_int($document->find('#price>.params_field_value')[0]->text());
        $description = remove_whitespace($document->find('#item_text_value')[0]->innerHtml());
        $location = self::field_value($document, '#city_name>.params_field_value');
        $year = self::field_value($document, '#field_custom_year>.params_field_value');
        $mileage = str_to_int(self::field_value($document, '#field_custom_milage>.params_field_value'));
        $body_type = self::field_value($document, '#field_custom_bodyType>.params_field_value');
        $transmission = self::field_value($document, '#field_custom_transmission>.params_field_value');
        $fuel_type = self::field_value($document, '#field_custom_engineType>.params_field_value');
        $drive_type = self::field_value($document, '#field_custom_driveType>.params_field_value');
        $seller_name = self::field_value($document, '#contact_name>#contact_field_value');
        $seller_phone = self::field_value($document, '#contact_phone>#contact_field_value');
        $link = $document->find('link')[0]->attr('href');
        $thumbnail = @$document->find('#mainPic');
        $thumbnail = $thumbnail ? $thumbnail[0]->attr('src') : null;
        $thumbs = $document->find('img.thumb');
        foreach ($thumbs as $thumb) {
            $remote = str_replace('t.jpg', '.jpg', $thumb->attr('src'));
            $path = 'cars/' . $ad_id . '_' . substr($remote, -5);
            if (Storage::disk()->put($path, Http::get($remote)->body())) {
                $images[] = "storage/$path";
            }
        }
        $posted_at = carbon(self::field_value($document, '#item_date'));

        $car_details =  [
            'title' => $title,
            'price' => $price,
            'description' => $description,
            'location' => $location,
            'year' => $year,
            'mileage' => $mileage,
            'body_type' => $body_type,
            'transmission' => $transmission,
            'fuel_type' => $fuel_type,
            'drive_type' => $drive_type,
            'seller_name' => substr($seller_name, 0, 31),
            'seller_phone' => $seller_phone,
            'url' => $link,
            'thumbnail' => $thumbnail,
            'images' => $images,
            'is_details_fetched' => true,
            'posted_at' => $posted_at,
        ];

        $car = self::firstOrCreate(['ad_id' => $ad_id], $car_details);
        $car->update($car_details);
        return $car;
    }

    /**
     * Get the string value of a parameter
     *
     * @param Document $document
     * @param string $selector
     * @param string $format
     * @return string|null
     * @throws \DiDom\Exceptions\InvalidSelectorException
     */
    public static function field_value(Document $document, string $selector, string $format = 'text')
    {
        $res = $document->find($selector);
        if ($res && $res[0]) {
            return remove_whitespace($res[0]->$format());
        } else {
            return null;
        }
    }

    /**
     * Use ad_id to in routes
     * @return string
     */
    public function getRouteKeyName(){
        return 'ad_id';
    }
}
