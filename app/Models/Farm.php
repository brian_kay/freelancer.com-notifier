<?php

namespace App\Models;

use App\Casts\Json;
use DiDom\Document;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class Farm extends Model
{
    use HasFactory, SoftDeletes;
    protected $guarded = [];

    protected $casts = [
        'images' => Json::class,
        'agents' => Json::class,
        'overview' => Json::class,
        'points_of_interest' => Json::class,
    ];

    /**
     * Fetch latest listings that meet a given set of rules
     * @return array
     * @throws \DiDom\Exceptions\InvalidSelectorException
     */
    public static function fetch()
    {
        $url = 'https://www.sahometraders.co.za/farms-for-sale?fromprice=100000&toprice=4000000&sortorder=ageascending&provinceids=7,9,8&page=';
        $range = [1, 1];
        $farms = [];
        for ($page = $range[0]; $page <= $range[1]; $page++) {
            $html = http_get($url . $page)->body();
            $document = new Document();
            $document->loadHtml($html);
            $date = null;
            $items = $document->find('.js_listingTile');

            foreach ($items as $index => $item) {
                $price = trim($item->find('.p24_price')[0]->text());
                $size = self::field_value($item, '.p24_size>span');
                if ($price === 'POA' || !$size)
                    continue;
                $price = str_to_int($price);
                $ad_id = $item->attr('data-listing-number');
                $thumbnail = @$item->find('.js_listingTileImageHolder .pull-left');
                $thumbnail = $thumbnail ? $thumbnail[0]->attr('src') : null;
                $link = @$item->find('a')[0]->attr('href');
                $location = self::field_value($item, '.p24_location');

                if ($price > 4000000)
                    continue;

                $farm = self::firstOrCreate(
                    ['ad_id' => $ad_id],
                    [
                        'price' => $price,
                        'title' => "Farm in $location",
                        'description' => self::field_value($item, '.p24_excerpt'),
                        'area' => $location,
                        'url' => $link ? "https://www.sahometraders.co.za$link" : null,
                        'size' => str_to_int($size),
                        'thumbnail' => $thumbnail,
                        'posted_at' => $date
                    ]
                );

                if ($farm->wasRecentlyCreated)
                    $farms[] = $farm;
            }
        }
        sleep(rand(2, 9));
        return $farms;
    }

    public static function fetch_details(string $string) : self|null|string
    {
        $images = $overview = $agents = $points_of_interest = [];
        $size = 0;

        if (str_contains($string, 'http')) {
            $ad_id = substr($string, -9);
            $url = $string;
        } else {
            return null;
        }

        try {
            $html = http_get($url)->body();
            $document = new Document();
            $document->loadHtml($html);
        } catch (\RuntimeException $e) {
            echo $e->getMessage(); //TODO: Do this properly
            return null;
        }

        $title = self::field_value($document, 'title');
        $description = self::field_value($document, '.sc_panelWrapListing .p24_listingCard', 'innerHtml');
        $contacts = $document->find('.agentTitle');
        $agency = self::field_value($document, '.p24_agency span');
        $province = self::field_value($document, 'ul.breadcrumb li:nth-child(5) a');
        $city = self::field_value($document, 'ul.breadcrumb li:nth-child(7) a');
        $area = self::field_value($document, 'ul.breadcrumb li:nth-child(9) a');
        $thumbs = $document->find('.js_galleryThumbnail');

        foreach ($document->find('.p24_propertyOverviewRow') as $row) {
            $key = self::field_value($row, '.p24_propertyOverviewKey');
            if ($key)
                $overview[$key] = self::field_value($row, '.p24_info');
        }

        foreach ($contacts as $contact) {
            $text = trim($contact->text());
            preg_match('/(.*)\r|\n/', $text, $ca);
            preg_match('/Telephone"\s,\sAgentId:\s"(.*)\"/', $text, $ids);
            $agents[$ids[1]] = $ca[1];
        }

        foreach ($document->find('.poiItem') as $row) {
            $key = self::field_value($row, '.poiItemName');
            if ($key)
                $points_of_interest[$key] = self::field_value($row, '.poiItemDistance');
        }

        $size = str_contains($overview['Size of farm'], 'm') ? str_to_int($overview['Size of farm']) / 10000 : str_to_int($overview['Size of farm']);

        $farm_details =  [
            'title' => $title,
            'description' => $description,
            'province' => $province,
            'city' => $city,
            'area' => $area,
            'agents' => $agents,
            'agency' => $agency,
            'size' => $size,
            'overview' => $overview,
            'points_of_interest' => $points_of_interest,
            'is_details_fetched' => true,
            'posted_at' => carbon($overview['List Date'])->toDateTimeString(),
        ];

        $farm = self::firstOrCreate(['ad_id' => $ad_id], $farm_details);

        foreach ($thumbs as $thumb) {
            $remote = substr($thumb->attr('lazy-src'), 0, -12);
            $path = 'farms/' . $ad_id . '/' . substr($remote, -9) . '.jpg';
            if (Storage::disk()->put($path, http_get($remote)->body())) {
                $images[] = "storage/$path";
            }
        }
        $farm_details['images'] = $images;

        $farm->update($farm_details);
        return $farm;
    }

    /**
     * Get the string value of a parameter
     *
     * @param Document|\DiDom\Element $document
     * @param string $selector
     * @param string $format
     * @return string|null
     * @throws \DiDom\Exceptions\InvalidSelectorException
     */
    public static function field_value(Document|\DiDom\Element $document, string $selector, string $format = 'text')
    {
        $res = $document->first($selector);
        if ($res) {
            return remove_whitespace($res->$format());
        } else {
            return null;
        }
    }

    /**
     * Use ad_id to in routes
     * @return string
     */
    public function getRouteKeyName(){
        return 'ad_id';
    }
}
