<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewProjects extends Mailable
{
    use Queueable, SerializesModels;

    public array $projects;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($projects)
    {
        $this->projects = $projects;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $count = count($this->projects);
        $s = $count !== 1 ? 's' : '';
        $subject = "$count new project$s on Freelancer.com";
        return $this->subject($subject)
            ->view('emails.new-projects');
    }
}
