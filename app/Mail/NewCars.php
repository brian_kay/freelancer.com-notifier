<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class NewCars extends Mailable
{
    use Queueable, SerializesModels;

    public array $cars;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($cars)
    {
        $this->cars = $cars;
    }

    /**
     * Get the message envelope.
     *
     * @return \Illuminate\Mail\Mailables\Envelope
     */
    public function envelope()
    {
        return new Envelope(
            subject: count($this->cars) . ' new car listing' . count($this->cars) > 1 ? 's' : '',
        );
    }

    /**
     * Get the message content definition.
     *
     * @return \Illuminate\Mail\Mailables\Content
     */
    public function content()
    {
        return new Content(
            view: 'emails.new-cars',
        );
    }
}
