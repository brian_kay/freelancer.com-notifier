<?php
/**
 * Remove multiple spaces and  new line characters
 * @param string $string
 * @return string
 */
function remove_whitespace(string $string)
{
    $string = preg_replace('/\s+/', ' ', $string);
    return trim($string);
}

/**
 * Return full page name
 * @return string
 */
function page_name($title = null)
{
    if ( ! $title ) {
        $title = str_replace( '.', ' ', \Request::route()->getName());
        $title = $title ?: str_replace('\\', ' ', \Request::path());
    }
    return ucfirst(str_replace(['-', '_'], ' ', $title));
}

/**
 * Return full page title
 * @return string
 */
function page_title($title = null)
{
    return page_name($title) . ' | ' . config('app.name') . ' - Manage property the easy way';
}


/**
 * Generate associative array from database results
 *
 * @param Illuminate\Database\Eloquent\Collection $data
 * @return array
 */
function id_assoc( $data, $value_column )
{
    $array = [];
    foreach ( $data as $row ) {
        $array[$row->id] = $row->{$value_column};
    }
    return $array;
}

/**
 * Turn a given string into a slug
 *
 * @param string $string
 * @param string $glue
 * @return string
 */
function slug( $string, $glue = '_' )
{
    $string = preg_replace("/[^a-zA-Z0-9\s]/", $glue, $string);
    $string = strtolower( str_replace( ' ', $glue, $string ) );
    return $string;
}

/**
 * Turn a slug into human friendly string
 *
 * @param string $string
 * @param string $glue
 * @return string
 */
function unslug( $string )
{
    $string = str_replace( ['-', '_'], ' ', $string );
    return $string;
}

/**
 * Format an amount of money
 * @param float $amount
 * @return string
 */
function money( float $amount )
{
    return 'R' . number_format( $amount, 2, '.', ',' );
}

/**
 * Format number
 * @param float|int $number
 * @return string
 */
function human_int( $number )
{
    return number_format( $number, 0, '.', ',' );
}

/**
 * Format a date
 * @param string $string
 * @return string
 */
function human_date(\Carbon\Carbon|string|null $string )
{
    if ($string) {
        if ($string instanceof \Carbon\Carbon) {
            $time = $string->timestamp;
        } else {
            $time = strtotime( $string );
        }
       return date( 'd M Y', $time );
    }

    return 'not provided';
}

/**
 * Format a date
 * @param string $string
 * @return string
 */
function human_datetime( $string )
{
    if ($string) {
        if ($string instanceof \Carbon\Carbon){
          $time = $string;
        }
        else {
            $time = \Carbon\Carbon::create($string);
        }
        $format = $time->isBefore(\Carbon\Carbon::create('today')) || $time->isAfter(\Carbon\Carbon::create('tomorrow')) ? 'd M Y H:i' : 'H:i';
        return $time->format($format);
    }

    return  'not provided';
}

/**
 * Create a carbon instance from a time string
 * @param string $string
 * @return \Carbon\Carbon
 */
function carbon( $string = null )
{
    if ($string instanceof \Carbon\Carbon)
    {
        return $string;
    }

    return $string ? \Carbon\Carbon::create( $string ) : \Carbon\Carbon::now();
}

/**
 * Return Carbon instance for current time
 * @return \Carbon\Carbon
 */
function carbon_now()
{
    return \Carbon\Carbon::now();
}

if (!function_exists('str_to_int')) {
    function str_to_int(string|null $string) {
        $string = $string ?: '';
        return (int)preg_replace('/([^0-9\\.])/i', '', $string);
    }
}

if (!function_exists('http_get')) {
    /**
     * @param string $url
     * @return Illuminate\Support\Facades\Http
     */
    function http_get(string $url) : Illuminate\Http\Client\Response
    {
        return \Illuminate\Support\Facades\Http::withHeaders(['User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; rv:111.0) Gecko/20100101 Firefox/111.0'])
                ->get($url);
    }
}

/**
 * Replace <br> with a new line in a string
 * @return string
 */
function br2nl( string $string )
{
    $string = str_replace( ['<br>', '<br/>', '<br />', '<BR>', '<BR/>', '<BR />'], "\n", $string );
    return $string;
}

/**
 * Explode with multiple delimiters
 * @param string $string
 * @return string
 */
function multiexplode( array $delimiters, string $string )
{
    $string = str_replace( $delimiters, $delimiters[0], $string );
    return explode( $delimiters[0], $string );
}

/**
 * Settings Helper
 * @param string $key
 * @return string
 */
function settings(string $key){
    if ($setting = \App\Models\Setting::where('name', $key)->first()){
        return $setting->value;
    }

    return null;
}

if (! function_exists('upload_image') ) {
    /**
     * Save uploaded image with a specified path and quality
     * @param Illuminate\Http\UploadedFile|string $file
     * @param string $path
     * @param int $quality
     * @return string
     */
    function upload_image($file, string $path = 'storage/images', int $quality = 80 )
    {
        $img = \Image::make($file instanceof \Illuminate\Http\UploadedFile ? $file->getRealPath() : $file);
        $img->resize(1200, 1200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        if (!file_exists(public_path($path))) {
            if (!mkdir($concurrentDirectory = public_path($path),777,true) && !is_dir($concurrentDirectory)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created',$concurrentDirectory));
            }
        }

        $path = rtrim( $path, '/') . '/' . time() . '_' . Str::random(4) . '.jpg';

        $img->save( $path, $quality );

        return $path;
    }
}


if (! function_exists('copy_files') ) {
    /**
     * Copy a file from one dir to another
     * @param string $file
     * @param string $path
     * @param string|null $file_name
     * @return string
     */
    function copy_files(string $file, string $path, string $file_name = null)
    {
        if (!file_exists($path)) {
            if (!mkdir($concurrentDirectory = $path,777,true) && !is_dir($concurrentDirectory)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created',$concurrentDirectory));
            }
        }

        $file_name = $file_name ?: ($split_array = explode('/', $file))[array_key_last($split_array)];

        $path = rtrim( $path, '/') . '/' . $file_name;

        \Illuminate\Support\Facades\File::copy($file, $path);

        return ltrim(str_replace([storage_path(), public_path()], '', $path), '/\\');
    }
}

/**
 * Convert total minutes to hour format
 *  110 -> 01:50
 */
function MinutesToHoursMins($time, $format = '%02d:%02d')
{
    if (!is_numeric($time))
    {
        return sprintf($format, '00', '00');
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}
/**
 * Convert hour format to total minutes
 *  1:50 -> 110
 */
function HoursMinsToMinutes($time)
{
    $time = explode(':', $time);
    return ((is_numeric($time[0]) ? $time[0] : 0 ) * 60) + (is_numeric(($time[1] ?? 0)) ? ($time[1] ?? 0) : 0 );
}

/**
 * Multi-byte version of substr_replace() in PHP 7+
 */
if (function_exists('mb_substr_replace') === false)
{
    function mb_substr_replace($string, $replacement, $start, $length = null, $encoding = null)
    {
        if (extension_loaded('mbstring') === true)
        {
            $string_length = (is_null($encoding) === true) ? mb_strlen($string) : mb_strlen($string, $encoding);

            if ($start < 0)
            {
                $start = max(0, $string_length + $start);
            }
            else
            if ($start > $string_length)
            {
                $start = $string_length;
            }

            if ($length < 0)
            {
                $length = max(0, $string_length - $start + $length);
            }
            else
            if ((is_null($length) === true) || ($length > $string_length))
            {
                $length = $string_length;
            }

            if (($start + $length) > $string_length)
            {
                $length = $string_length - $start;
            }

            if (is_null($encoding) === true)
            {
                return mb_substr($string, 0, $start) . $replacement . mb_substr($string, $start + $length, $string_length - $start - $length);
            }

            return mb_substr($string, 0, $start, $encoding) . $replacement . mb_substr($string, $start + $length, $string_length - $start - $length, $encoding);
        }

        return (is_null($length) === true) ? substr_replace($string, $replacement, $start) : substr_replace($string, $replacement, $start, $length);
    }
}
