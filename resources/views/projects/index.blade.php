@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                @foreach($projects as $project)
                    <div class="card my-3">
                        <div class="card-header row">
                            <div class="col-md-8">
                                <strong><a href="https://freelancer.com/projects/{{ $project->fl_id }}.html" target="_blank">{{ $project->title }}</a></strong>
                            </div>
                            <div class="col-md-4 text-md-end">Posted: {{ date('M-d H:i', $project->time_submitted) }}</div>
                        </div>
                        <div class="card-body">
                            <p>{!! $project->description !!}</p>
                            <a href="https://freelancer.com/users/{{ $project->client->fl_id }}.html" class="card-link">{{ $project->client->public_name ?? $project->client->username ?? '' }}</a>
                            <p class="text-muted">City: {{ $project->client->city }} | Reputation: {{ $project->client->reputation }}/5</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop
