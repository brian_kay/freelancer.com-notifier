@extends('layouts.app')

@section('content')
    <style xmlns="http://www.w3.org/1999/html">
        .sticky-buttons {
            position: fixed;
            bottom: 20px;
            left: 30px;
            width: 200px;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="header my-5">
                    <h2 class="page-title text-center">
                        {{ $car->title }}
                    </h2>
                </div>
                <div class="card my-3">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-8">
                                <strong><a href="{{ $car->url }}" target="_blank">{{ $car->title }}</a></strong>
                            </div>
                            <div class="col-md-4 text-md-end">Price: {{ money($car->price) }}</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @forelse($car->images ?? [] as $image)
                                <div class="col-md-6 col-md-4">
                                    <img src="{{ asset($image) }}" alt="Thumbnail" class="w-100 mb-3    ">
                                </div>
                            @empty
                                 <div class="col-md-6 col-md-4">
                                    <img src="{{ $car->thumbnail }}" alt="Thumbnail" class="w-100">
                                </div>
                            @endforelse
                        </div>
                        <div class="row">
                            <div class="col-md-4 car-details">
                                <h3>Details</h3>
                                <ul class="no-style">
                                    <li>City: <span>{{ $car->location }}</span></li>
                                    <li>Posted: <span>{{ human_date($car->posted_at) }}</span></li>
                                    <li>Price: <span>{{ money($car->price) }}</span></li>
                                    <li>Mileage: <span>{{ number_format($car->mileage) }}km</span></li>
                                    <li>Year: <span>{{ $car->year }}</span></li>
                                    <li>Body type: <span>{{ $car->body_type }}</span></li>
                                    <li>Transmission: <span>{{ $car->transmission }}</span></li>
                                    <li>Fuel type: <span>{{ $car->fuel_type }}</span></li>
                                    <li>Drive type: <span>{{ $car->drive_type }}</span></li>
                                    <li>Seller name: <span>{{ $car->seller_name }}</span></li>
                                    <li>Seller phone: <span><a href="tel:{{ $car->seller_phone }}">{{ $car->seller_phone }}</a></span></li>
                                    <li>Details fetched: <span>{{ $car->is_details_fetched ? 'Yes' : 'No' }}</span></li>
                                </ul>
                                <button role="button" onclick="bookmark({{ $car->ad_id }})" class="btn btn-outline-{{ $car->is_bookmarked ? 'secondary' : 'info' }}" id="btn-bkmk-{{ $car->ad_id }}">{{ $car->is_bookmarked ? 'Remove ' : '' }}Bookmark</button>
                            </div>
                            <div class="col-md-8">
                                <h3>Description</h3>
                                {!! $car->description !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sticky-buttons">
        <a class="btn btn-outline-info d-block mb-3" href="/cars">Page 1</a>
        <a class="btn btn-outline-info d-block" href="/cars/new" target="_blank">Fetch</a>
    </div>
    <script>
        function fetch_details() {
            fetch('/cars/fetch_details');
            var rand = Math.floor(Math.random() * (60 - 3 + 1) + 3);
            console.log('Waiting for ' + rand + ' seconds');
            setTimeout(fetch_details, rand * 1000);
        }
        async function bookmark(ad_id) {
            const res = await fetch('/cars/' + ad_id + '/bookmark')
            if (res.ok) {
                let btn = $('#btn-bkmk-' + ad_id);
                btn.toggleClass('btn-outline-secondary btn-outline-info');
                let text = btn.text()
                btn.text(text.length > 9 ? 'Bookmark' : 'Remove bookmark')
            }
        }

        fetch_details()
    </script>
