@extends('layouts.app')

@section('content')
    <style>
        .sticky-buttons {
            position: fixed;
            bottom: 20px;
            left: 30px;
            width: 200px;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="header my-5">
                    <h2 class="page-title text-center">
                        {{ count($cars) }} Cars
                    </h2>
                </div>
                @foreach($cars as $car)
                    <div class="card my-3">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-8">
                                    <strong><a href="{{  route('cars.show', $car) }}" target="_blank">{{ $car->title }}</a></strong>
                                    <a href="{{ $car->url }}" target="_blank">Source</a>
                                </div>
                                <div class="col-md-4 text-md-end">Price: {{ money($car->price) }}</div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="{{ $car->images ? asset($car->images[0]) : $car->thumbnail }}" alt="Thumbnail" class="w-100">
                                </div>
                                <div class="col-md-8">
                                    <div>{!! $car->description !!}</div>
                                    <p class="text-muted">City: {{ $car->location }} | Posted: {{ human_date($car->posted_at) }}</p>
                                    <button role="button" onclick="bookmark({{ $car->ad_id }})" class="btn btn-outline-{{ $car->is_bookmarked ? 'secondary' : 'info' }}" id="btn-bkmk-{{ $car->ad_id }}">{{ $car->is_bookmarked ? 'Remove ' : '' }}Bookmark</button>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
                @if(!is_array($cars))
                    <div class="pagination-links">
                        {!! $cars->links() !!}
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="sticky-buttons">
        <a class="btn btn-outline-info d-block mb-3" href="/cars">Page 1</a>
        <a class="btn btn-outline-info d-block" href="/cars/new" target="_blank">Fetch</a>
    </div>
    <script>
        function fetch_details() {
            fetch('/cars/fetch_details');
            var rand = Math.floor(Math.random() * (60 - 3 + 1) + 3);
            console.log('Waiting for ' + rand + ' seconds');
            setTimeout(fetch_details, rand * 1000);
        }
        async function bookmark(ad_id) {
            const res = await fetch('/cars/' + ad_id + '/bookmark')
            if (res.ok) {
                let btn = $('#btn-bkmk-' + ad_id);
                btn.toggleClass('btn-outline-secondary btn-outline-info');
                let text = btn.text()
                btn.text(text.length > 9 ? 'Bookmark' : 'Remove bookmark')
            }
        }

        fetch_details()
    </script>
@stop
