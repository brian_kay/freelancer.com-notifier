@extends('layouts.app')

@section('content')
    <style>
        .sticky-buttons {
            position: fixed;
            bottom: 20px;
            left: 30px;
            width: 200px;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="header my-5">
                    <h2 class="page-title text-center">
                        Farms
                    </h2>
                </div>
                @foreach($farms as $farm)
                    <div class="card my-3">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-8">
                                    <strong><a href="{{  route('farms.show', $farm) }}" target="_blank">{{ $farm->title }}</a></strong>
                                    <a href="{{ $farm->url }}" target="_blank">Source</a>
                                </div>
                                <div class="col-md-4 text-md-end">Price: {{ money($farm->price) }}</div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="{{ asset($farm->images ? $farm->images[0] : 'images/blank.svg') }}" alt="Thumbnail" class="w-100">
                                </div>
                                <div class="col-md-8">
                                    <div>{!! $farm->description !!}</div>
                                    <p class="text-muted">Location: {{ implode(', ', [$farm->province , $farm->city, $farm->area]) }} | Posted: {{ human_date($farm->posted_at) }}</p>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
                @if(!is_array($farms))
                    <div class="pagination-links">
                        {!! $farms->links() !!}
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="sticky-buttons">
        <a class="btn btn-outline-info d-block mb-3" href="/farms">Page 1</a>
        <a class="btn btn-outline-info d-block" href="/farms/new" target="_blank">Fetch</a>
    </div>
    <script>
        function fetch_details() {
            fetch('/farms/fetch_details');
            var rand = Math.floor(Math.random() * (120 - 45 + 1) + 45);
            console.log('Waiting for ' + rand + ' seconds');
            setTimeout(fetch_details, rand * 1000);
        }

        fetch_details()
    </script>
@stop
