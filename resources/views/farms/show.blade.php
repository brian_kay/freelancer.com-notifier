@extends('layouts.app')

@section('content')
    <style xmlns="http://www.w3.org/1999/html">
        .sticky-buttons {
            position: fixed;
            bottom: 20px;
            left: 30px;
            width: 200px;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="header my-5">
                    <h2 class="page-title text-center">
                        {{ $farm->title }}
                    </h2>
                </div>
                <div class="card my-3">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-8">
                                <strong><a href="{{ $farm->url }}" target="_blank">{{ $farm->title }}</a></strong>
                            </div>
                            <div class="col-md-4 text-md-end">Price: {{ money($farm->price) }}</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 car-details">
                                <h3>Details</h3>
                                <ul class="no-style">
                                    <li>Location: <span>{{ implode(', ', [$farm->province , $farm->city, $farm->area]) }}</span></li>
                                    <li>Posted: <span>{{ human_date($farm->posted_at) }}</span></li>
                                    <li>Price: <span>{{ money($farm->price) }}</span></li>
                                    <li>Size: <span>{{ number_format($farm->size) }}ha</span></li>
                                    <li>Agency: <span>{{ $farm->agency }}</span></li>
                                    <li>Agent: <span>{{ $farm->agent }}</span></li>
                                    <li>Details fetched: <span>{{ $farm->is_details_fetched ? 'Yes' : 'No' }}</span></li>
                                </ul>
                                <a href="{{ route('cars.bookmark', $car) }}" class="btn btn-outline-{!! $car->is_bookmarked ? 'secondary">Remove ' : 'info">' !!}Bookmark</a>
                            </div>
                            <div class="col-md-8">
                                <h3>Description</h3>
                                {!! $farm->description !!}
                                <h3>Overview</h3>
                                <ul class="no-style">
                                    @foreach($farm->overview as $key => $item)
                                        <li>{{ $key }}: <span>{{ $item }}</span></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            @forelse($farm->images ?? [] as $image)
                                <div class="col-md-6 col-md-4">
                                    <img src="{{ asset($image) }}" alt="Thumbnail" class="w-100 mb-3    ">
                                </div>
                            @empty
                                 <div class="col-md-6 col-md-4">
                                    {{-- <img src="{{ $farm->thumbnail }}" alt="Thumbnail" class="w-100"> --}}
                                </div>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sticky-buttons">
        <a class="btn btn-outline-info d-block mb-3" href="/cars">Page 1</a>
        <a class="btn btn-outline-info d-block" href="/cars/new" target="_blank">Fetch</a>
    </div>
    <script>
        function fetch_details() {
            fetch('/farms/fetch_details');
            var rand = Math.floor(Math.random() * (60 - 3 + 1) + 3);
            console.log('Waiting for ' + rand + ' seconds');
            setTimeout(fetch_details, rand * 1000);
        }

        fetch_details()
    </script>
@stop
