#!/bin/bash
docker build -t kajuzi/php8-apache-bookworm:1.0 /var/www/staging/docker/
docker rm -f scraper
docker run -d -p 8082:80 --name=scraper --restart=unless-stopped --network=kajuzi -v /var/run/docker.sock:/var/run/docker.sock -v /var/www/scraper:/var/www kajuzi/php8-apache-bookworm:1.0
docker ps
