<?php

use App\Http\Controllers\CarsController;
use App\Http\Controllers\FarmsController;
use App\Http\Controllers\ProjectsController;
use Illuminate\Support\Facades\Route;

Route::get('cars/fetch_details', [CarsController::class, 'fetch_details']);
Route::get('cars/{id}/bookmark', [CarsController::class, 'bookmark'])->name('cars.bookmark');
Route::get('cars/bookmarks', [CarsController::class, 'bookmarks'])->name('cars.bookmarks');
Route::get('farms/fetch_details', [FarmsController::class, 'fetch_details']);
Route::resource('projects', ProjectsController::class);
Route::resource('cars', CarsController::class);
Route::resource('farms', FarmsController::class);
