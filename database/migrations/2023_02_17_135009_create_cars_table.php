<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ad_id', 32)->unique()->index();
            $table->string('title');
            $table->integer('price')->default(0);
            $table->text('description')->nullable();
            $table->string('location', 64)->nullable();
            $table->unsignedInteger('year')->nullable();
            $table->unsignedInteger('mileage')->nullable();
            $table->string('body_type', 32)->nullable();
            $table->string('transmission', 32)->nullable();
            $table->string('fuel_type', 32)->nullable();
            $table->string('drive_type', 32)->nullable();
            $table->string('seller_name', 64)->nullable();
            $table->string('seller_phone', 32)->nullable();
            $table->string('seller_id', 32)->nullable();
            $table->string('url')->nullable();
            $table->string('thumbnail')->nullable();
            $table->json('images')->nullable();
            $table->boolean('is_details_fetched')->default(false);
            $table->boolean('is_bookmarked')->nullable();
            $table->timestamp('posted_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
};
