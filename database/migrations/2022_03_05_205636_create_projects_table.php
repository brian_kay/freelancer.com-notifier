<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('fl_id')->unique()->index();
            $table->string('title');
            $table->string('preview_description');
            $table->text('description');
            $table->string('currency', 3)->default('USD');
            $table->integer('budget_min')->default(0);
            $table->integer('budget_max')->default(0);
            $table->integer('deadline')->nullable();
            $table->integer('client_id')->nullable();
            $table->integer('time_submitted')->nullable();
            $table->integer('time_updated')->nullable();
            $table->json('other_details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
};
