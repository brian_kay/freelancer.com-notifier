<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->integer('fl_id')->unique()->index();
            $table->string('username', 64)->unique();
            $table->string('display_name', 64)->nullable();
            $table->string('public_name', 64)->nullable();
            $table->string('country', 2)->nullable();
            $table->string('city', 64)->nullable();
            $table->float('reputation')->nullable();
            $table->json('status')->nullable();
            $table->integer('registration_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
};
