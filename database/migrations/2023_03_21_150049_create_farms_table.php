<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farms', function (Blueprint $table) {
            $table->id();
            $table->integer('ad_id')->unique();
            $table->string('title');
            $table->integer('price')->default(0);
            $table->text('description')->nullable();
            $table->string('province', 32)->nullable();
            $table->string('city', 32)->nullable();
            $table->string('area', 64)->nullable();
            $table->unsignedFloat('size')->nullable();
            $table->unsignedInteger('bedrooms')->nullable();
            $table->unsignedInteger('bathrooms')->nullable();
            $table->json('overview')->nullable();
            $table->string('agency', 64)->nullable();
            $table->json('agents')->nullable();
            $table->string('url')->nullable();
            $table->string('thumbnail')->nullable();
            $table->json('images')->nullable();
            $table->json('points_of_interest')->nullable();
            $table->string('video', 100)->nullable();
            $table->boolean('has_map')->default(0);
            $table->boolean('is_details_fetched')->default(false);
            $table->boolean('is_bookmarked')->nullable();
            $table->timestamp('posted_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farms');
    }
};
